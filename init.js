/*
 * All requires
 */

const sequencerModule = require('./sequencer');
const redis = require('redis');
const jsonpack = require('jsonpack/main');

/*
 * Initialize all needed process and Sequencer,
 * For the detections orchestrations
 */

module.exports.Init = class Init {

    constructor(config) {

        this.wsServer = config.wsServer;
        this.db = config.db;
        this.dumpSize = 5;
        this.io = require('socket.io')(this.wsServer);
        this.sequencer = new sequencerModule.Sequencer(this.db, 'public/sequences', this.dumpSize);
        this.redis = require('redis');
        this.client = redis.createClient();

        this.client.flushall(() => {
            console.log("[INIT] Flush all Redis database");
        });
        /* Init active cameras */
        this.db.collection('active_cameras').find({}).toArray((err, active_cameras) => {
            active_cameras.forEach((active_camera) => {
                this.db.collection('detections').find({'assoc': active_camera.jsonRes._id}).count((err, count) => {
                    count *= 1000;
                    this.sequencer.add(active_camera.jsonRes, count);
                });
            });
        });

        /* Init alarms */
        this.db.collection('areas').find({'isAlarm': true}).toArray((err, areas) => {
            areas.forEach((area) => {
                this.client.hset('visiolab-vision-' + area.assoc + '-alarms', area.name, JSON.stringify(area));
            });
        });
    }

    startWsServer() {

        this.sequencer.player.on('result', (data) => {
            this.io.emit('dets', data);
        });

    }

    startAll() {

        this.startWsServer();
        this.sequencer.start();

    }

};
