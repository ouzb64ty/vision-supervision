const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/vision';

/* Database ------------------------------------------------------- */

(async function() {
    try {

        /* Requires & Variables ------------------------------------------ */

        const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true } );
        const express = require('express');
        const cors = require('cors');
        const api = express();
        const bodyParser = require('body-parser');
        const resources = require('./resources');
        const db = await client.db();
        const wsServer = require('http').Server(api);
        const collectionsModule = require('./models/collections');
        const dumpSize = 300;
        const initModule = require('./init');
        const init = new initModule.Init({

            'dumpSize': dumpSize,
            'wsServer': wsServer,
            'db': db

        });
        const collections = new collectionsModule(api, db, init.sequencer);

        /* Initialize WS server & camera sequencer ------------------------- */

        init.startAll();

        /* Express configuration ------------------------------------------ */

        api.use(express.static('public'));
        api.use(cors());
        api.use(bodyParser.json());
        api.use(bodyParser.urlencoded({ extended: true }));

        /* Get Bearer token for auth -------------------------------------- */

        function managePrivileges(req, res, next) {

            const bearerHeader = req.headers["authorization"];

            if (typeof bearerHeader != 'undefined') {
                const bearer = bearerHeader.split(" ");
                const bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.statusCode = 401;
                res.json({
                    'error': 'Need correct API token in Bearer token'
                });
            }
        }

        /* API */

        collections.create("DELETE", '/vision/v1/areas/:id', managePrivileges, resources.API.deleteArea);
        collections.create("GET", '/vision/v1/areas/:id', managePrivileges, resources.API.getCameraAreas);
        collections.create("GET", '/vision/v1/active_cameras/:n', managePrivileges, resources.API.getNActiveCameras);
        collections.create("GET", '/vision/v1/active_cameras', managePrivileges, resources.API.getActiveCameras);
        collections.create("GET", '/vision/v1/cameras/:id', managePrivileges, resources.API.getCamera);
        collections.create("POST", '/vision/v1/active_cameras', managePrivileges, resources.API.postActiveCameras);
        collections.create("GET", '/vision/v1/detections', managePrivileges, resources.API.getDetections);
        collections.create("GET", '/vision/v1/detections/cameras/:id/progressSec/:progressSec', managePrivileges, resources.API.getOneDetection);
        collections.create("GET", '/vision/v1/detections/cameras/:id/search/:n', managePrivileges, resources.API.postSearchCamerasDetections);
        collections.create("GET", '/vision/v1/detections/cameras/:id', managePrivileges, resources.API.getCameraDetections);
        collections.create("POST", '/vision/v1/areas', managePrivileges, resources.API.postAreas);
        collections.create("POST", '/vision/v1/detections', managePrivileges, resources.API.postDetections);
        collections.create("POST", '/vision/v1/export/:id', managePrivileges, resources.API.postExport);
        collections.create("DELETE", '/vision/v1/assoc/:id', managePrivileges, resources.API.deleteAssoc);
        collections.create("POST", '/vision/v1/alarm/:name', managePrivileges, resources.API.postAlarm);
        collections.create("GET", '/vision/v1/alarm/:name', managePrivileges, resources.API.getAlarm);
        collections.create("GET", '/vision/v1/alarms/all', managePrivileges, resources.API.getAlarms);
        collections.create("GET", '/vision/v1/alarms', managePrivileges, resources.API.getDetectionsAlarms);

        /* Port Binding --------------------------------------------------- */

        api.listen(8003);
        wsServer.listen(8002);

    } catch(e) {

        /* Database Error Connection ------------------------------------------ */

        console.error(e)

    }

})();
