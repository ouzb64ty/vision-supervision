/*
 * All requires
 */

var spawn = require("child_process").spawn;
const net = require('net');
const getVideoDimensions = require('get-video-dimensions');
const fs = require('fs');
const path = require('path');
const events = require('events');
const jsonpack = require('jsonpack/main');
const redis = require('redis');
const { getVideoDurationInSeconds } = require('get-video-duration');

module.exports.Sequencer = class Sequencer {

    /*
     * Need Database, sequences directory and Redis dump size before
     * MongoDB saving
     */

    constructor(db, path, dumpSize) {

        /* Config */
        this.cameras = [];
        this.redisClient = redis.createClient();
        this.dumpSize = dumpSize;
        this.detsNb = 0;
        this.cursor = 0;
        this.db = db;
        this.path = path;
        this.init = false;
        this.eventEmitter = new events.EventEmitter();
        this.player = new events.EventEmitter();
        this.deleted = '';
        /* Verification */
        if (!fs.existsSync(this.path))
            fs.mkdir(this.path, { recursive: true }, (err) => { if (err) throw err; });
        /* Connection to Supstream */
        this.supstream = new net.Socket();
        this.supstream.connect(14000, "127.0.0.1");
        this.supstream.on('data', (data) => {
            if (data.toString() == 'READY' && this.init == false) {
                this.player.emit('next');
                this.init = true;
            } else if (data.toString() == 'OK') {
                var path = require('path');
                var data = this.cameras[this.cursor];
                var sequencePath = path.join(data.dir, data.progress.toString()) + '.jpg';
                var supnetRequest = {
                    filename: '../' + this.sequencePath
                };

                console.log('[SEQUENCE from Supstream] ' + data.title + ' sequenced at ' + sequencePath + '.');
                this.supnetClient.write(JSON.stringify(supnetRequest));
            }
        });
        /* Connection to Supnet */
        this.supnetClient = new net.Socket();
        this.supnetClient.connect(1717, "127.0.0.1");
        this.supnetClient.on('data', (data) => {
            new Promise((resolve, reject) => {
                const jsonData = JSON.parse(data.toString());

                if (this.deleted == this.cameras[this.cursor])
                    reject();
                else
                    var nodeValue = this.cameras[this.cursor];

                jsonData.date = Date.now();
                jsonData.domain = nodeValue.title;
                jsonData.progressSec = nodeValue.progress;
                jsonData.url = nodeValue.url;
                jsonData.assoc = nodeValue.assoc;
                jsonData.src = "http://127.0.0.1:8002/" + this.sequencePath.substr(7, this.sequencePath.length);
                jsonData.dets = Sequencer.rectifyYoloCoords(jsonData.dets, nodeValue);
                jsonData.alarms = [];
                jsonData.isSeen = false;
                console.log('[SEQUENCE from Supnet] ' + nodeValue.title + ' sequence analysed.');
                /* Alarms */
                this.redisClient.hvals('visiolab-vision-' + jsonData.assoc + '-alarms', (err, replies) => {
                    if (err) throw err;

                    /* Time activity*/
                    var date = new Date();
                    var hour = date.getHours();
                    var min  = date.getMinutes();
                    var currentMS = (+hour * (60000 * 60)) + (+min * 60000);

                    for (var i = 0; i < replies.length; i++) {
                        let areaAlarm = JSON.parse(replies[i]);
                        var activity = false;

                        if (areaAlarm.timestampFrom > areaAlarm.timestampTo) {
                            if (currentMS <= areaAlarm.timestampTo || currentMS >= areaAlarm.timestampFrom)
                                activity = true;
                            else
                                activity = false;
                        } else {
                            if (currentMS >= areaAlarm.timestampFrom && currentMS <= areaAlarm.timestampTo)
                                activity = true;
                            else
                                activity = false;
                        }
                        if (activity == true) {
                            /* Dets in area */
                            if (Sequencer.detsInArea(jsonData.dets, areaAlarm) === true) {
                                jsonData.alarms.push([areaAlarm.name, areaAlarm.assoc]);
                                console.log("[SEQUENCE] Alert in " + nodeValue.title + " sequence in " + areaAlarm.name + " area.");
                            }
                        }
                    }
                    resolve(jsonData);
                });
            }).then((jsonData) => {
                this.player.emit('result', jsonData);
                this.detsNb += 1;
                this.save(jsonData);
                this.cameras[this.cursor].progress += 1000;
                if (this.cursor == this.cameras.length - 1)
                    this.cursor = 0;
                else
                    this.cursor += 1;
                this.player.emit('next');
            }).catch(() => {
                console.log("[SEQUENCE] Camera deleted during analyse.");
            });
        });

    }

    static strCatToIntCat(categories) {
        var classes = [];

        if (categories.includes('personne'))
            classes.push(0);
        else if (categories.includes('velo'))
            classes.push(1);
        else if (categories.includes('voiture'))
            classes.push(2);
        else if (categories.includes('moto'))
            classes.push(3);
        else if (categories.includes('bus'))
            classes.push(4);
        else if (categories.includes('camion'))
            classes.push(5);
        else if (categories.includes('sacados'))
            classes.push(6);
        else if (categories.includes('sacamain'))
            classes.push(7);
        else if (categories.includes('valise'))
            classes.push(8);
        else if (categories.includes('animal'))
            classes.push(9);
        return (classes);
    }

    static detsInArea(dets, area) {
        let areaClass = Sequencer.strCatToIntCat(area.categories);

        for (var i in dets) {
            if (areaClass.includes(dets[i].class)
                && ((dets[i].x >= area.coords[0]
                    && dets[i].x <= area.coords[0] + area.coords[2]
                    && dets[i].y >= area.coords[1]
                    && dets[i].y <= area.coords[1] + area.coords[3])
                    || (dets[i].x + dets[i].w >= area.coords[0]
                        && dets[i].x + dets[i].w <= area.coords[0] + area.coords[2]
                        && dets[i].y >= area.coords[1]
                        && dets[i].y <= area.coords[1] + area.coords[3])
                    || (dets[i].x >= area.coords[0]
                        && dets[i].x <= area.coords[0] + area.coords[2]
                        && dets[i].y + dets[i].h >= area.coords[1]
                        && dets[i].y + dets[i].h <= area.coords[1] + area.coords[3])
                    || (dets[i].x + dets[i].w >= area.coords[0]
                        && dets[i].x + dets[i].w <= area.coords[0] + area.coords[2]
                        && dets[i].y + dets[i].h >= area.coords[1]
                        && dets[i].y + dets[i].h <= area.coords[1] + area.coords[3]))) {

                return (true);
            }
        }
        return (false);
    }

    /* Coords Rectifications */

    static rectifyYoloCoords(dets_, node) {
        var dets = [];

        for (var key in dets_) {
            /* Pourc to pixels and rectify*/
            var x = dets_[key].xmin * node.width;
            var y = dets_[key].ymin * node.height;
            var w = (dets_[key].xmax - dets_[key].xmin) * node.width;
            var h = (dets_[key].ymax - dets_[key].ymin) * node.height;
            dets.push({
                "class": dets_[key].class,
                "color": dets_[key].color,
                "x": x,
                "y": y,
                "w": w,
                "h": h
            });
        }
        return (dets);
    }

    /*
     * Stat of analyse
     */

    isActive(assoc) {
        for (var i = 0; i < this.cameras.length; i++) {
            if (this.cameras[i].assoc == assoc) {
                if (this.cameras[i].isActive == 1)
                    return (true);
            }
        }
        return (false);
    }

    /*
     * Pause analyse
     */

    pause(assoc) {
        for (var i = 0; i < this.cameras.length; i++) {
            if (this.cameras[i].assoc == assoc) {
                this.cameras[i].isActive = 0;
                break ;
            }
        }
    }

    /*
     * Resume analyse
     */

    resume(assoc) {
        for (var i = 0; i < this.cameras.length; i++) {
            if (this.cameras[i].assoc == assoc) {
                this.cameras[i].isActive = 1;
                break ;
            }
        }
    }

    /*
     * Remove analyse
     */

    remove(assoc) {
        for (var i = 0; i < this.cameras.length; i++) {
            if (this.cameras[i].assoc == assoc) {
                this.cameras.splice(i, 1);
                if (this.cameras.length == 0)
                    this.init = false;
                break ;
            }
        }
    }

    add(activeCamera, count) {

        getVideoDimensions(activeCamera.url).then((dimensions) => {

            let dir = path.join(this.path, activeCamera.pathSequence);

            if (activeCamera.isActive == true) var isActive = 1;
            else var isActive = 0;

            let data = {
                'title': activeCamera.namespace,
                'url': activeCamera.url,
                'assoc': activeCamera._id,
                'progress': count,
                'width': dimensions.width,
                'height': dimensions.height,
                'dir': dir,
                'isActive': isActive
            };

            console.log("[SEQUENCER] " + activeCamera.namespace + " who correspond to " + activeCamera.url + " url is append.");
            if (!fs.existsSync(dir))
                fs.mkdir(dir, { recursive: true }, (err) => { if (err) throw err; });
            this.cameras.push(data);
            this.supstream.write('new ' + activeCamera.namespace + ' ' + activeCamera.url);
        }).catch((err) => {
            console.log("[SEQUENCER] Can't launch " + activeCamera.namespace + " RTSP stream.");
            /*this.db.collection('active_cameras').deleteOne({'jsonRes._id': activeCamera.assoc});*/
        });

    }

    /*
     * Save data detections in Redis DB
     */

    save(data) {

        let packed;

        packed = jsonpack.pack(data);
        if (data.res == 1)
            this.redisClient.rpush('visiolab-vision-detections', JSON.stringify(packed));
        if ((this.detsNb != 0 && this.detsNb % this.dumpSize == 0) || this.dumpSize == 1) {
            this.dumpCache();
        }

    }

    /*
     * Dump Redis DB in MongoDB
     */

    dumpCache() {

        let saveJSON = {};
        let arrayJSON = [];

        this.redisClient.lrange('visiolab-vision-detections', 0, this.dumpSize - 1, (err, replies) => {
            if (err) throw err;

            for (let i = 0; i < replies.length; i++) {
                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                arrayJSON.push(saveJSON);
            }
            if (arrayJSON.length > 0) {
                this.db.collection('detections').insertMany(arrayJSON, (err, dbResult) => {
                    if (err) throw err;

                    this.redisClient.del('visiolab-vision-detections');
                });
            }
        });

    }

    /*
     * Start player sequencer in _time_ milliseconds
     */

    start() {

        let data;
        let supnetRequest;
        let basename;

        this.player.on('next', () => {
            try {
                if (this.cameras && this.cameras.length > 0) {
                    data = this.cameras[this.cursor];
                    if (data.isActive == 1) {
                        this.sequencePath = path.join(data.dir, data.progress.toString()) + '.jpg';
                        supnetRequest = {
                            filename: '../' + this.sequencePath
                        };
                        /* wget = spawn('wget', ['-q', data.url, '-O', this.sequencePath]); */
                        /*wget.stdout.once('end', (code) => {
                            console.log("Sequence " + data.title + " from " + data.progress + "sec.");
                            this.supnetClient.write(JSON.stringify(supnetRequest));
                        });*/
                        this.supstream.write('grab ' + data.title + ' ../' + this.sequencePath);
                    } else {
                        setImmediate(() => {
                            if (this.cursor == this.cameras.length - 1)
                                this.cursor = 0;
                            else
                                this.cursor += 1;
                            this.player.emit('next');
                        });
                    }
                }
            } catch (err) {

                throw err;

            }

        });

    }

};

/*
const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/vision';

(async function() {
    try {

        const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true } );
        const db = await client.db();
        var sequencer = new Sequencer(db, 'public/sequences', 200);

        sequencer.start();
        sequencer.add('test1', 'http://185.10.80.33:8082/cgi-bin/faststream.jpg?stream=half&fps=15&rand=COUNTER', 'camera', 0);
        sequencer.add('test2', 'http://85.71.106.87:60001/oneshotimage1?1570540709', 'camera', 0);
        setTimeout(() => {
            sequencer.add('test3', 'http://176.57.73.231/mjpg/video.mjpg', 'camera', 0);
        }, 20000);

    } catch(e) {

        console.error(e)

    }

})();
*/